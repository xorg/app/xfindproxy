xfindproxy is used to locate available X11 proxy services.

It utilizes the Proxy Management Protocol to communicate with a proxy
manager.  The proxy manager keeps track of all available proxy
services, starts new proxies when necessary, and makes sure that
proxies are shared whenever possible.

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The master development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/app/xfindproxy

Please submit bug reports and requests to merge patches there.

For patch submission instructions, see:

  https://www.x.org/wiki/Development/Documentation/SubmittingPatches

